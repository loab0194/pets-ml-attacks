# Privacy Enhacing Technologies Project

This is our implementation for PETs Project SS-2021

## Running Attacks separately
- Each attack can be found in a separete notebook that includes training the target model, training the attack model, and visualizing the results.
- To run these notebooks:   
1 - Open the termial inside the Attacks_Jupyter_Notebooks folder  
2 - Run the following command  
```
jupyter notebook
```
3 - Open the notebook  
- Attribute Inference Attack Notebook:
```
Attribute_Inference.ipynb
```
- Membership Inference Attack Notebooks:
```
Membership_Inference_CIFAR10.ipynb
Membership_Inference_MNIST.ipynb
```
- Model Inversion Attack Notebook:
```
Model_Inversion.ipynb
```


## Running Attacks together

1 - Open terminal inside the folder TestingPlatform  
2 - Run the following command  
```
python3 main.py
```
3 - Follow the instructions of the terminal to select an attack, a target model path, and a dataset path

## More Details about using TestingPlatform

### Attribute Inference
- Enter "A" to execute Model Inversion Attack
- Enter the path of the target model (Default value is "./property_inference/model.pt")
- Enter 1 for CIFAR-10 and 2 for MNIST
- Enter the path of the test dataset (Default value is "./property_inference/UTKFace")
- Dataset should be downloaded from the following link:
https://drive.google.com/drive/folders/0BxYys69jI14kU0I1YUQyY1ZDRUE?resourcekey=0-01Pth1hq20K4kuGVkp3oBw
- Enter "M" to use the MLP Attack Model (faster) and "X" to use the XG Boost (Better results)
- To use XG Boost it must be installed with the same version of the saved model (0.90), use the following command to install it:
```
python3 -m pip install xgboost==0.90
```
- The attack result is printed in the terminal


### Model Inverion
- Enter "I" to execute Model Inversion Attack
- Enter path of the target model
- The attack results can be seen in "TestingPlatform/model_inversion_results"

### Membership Inference
- Enter "M" to execute Model Inversion Attack
- Enter the path of the target model (Default values are "./membership_inference/target_model_CIFAR.pt" or "./membership_inference/target_model_MNIST.pt")
- Enter 1 for CIFAR-10 and 2 for MNIST
- Enter the path of the test dataset (Default values are "./membership_inference/cifar_test_data" or "./membership_inference/mnist_test_data")
- The attack result is printed in the terminal
