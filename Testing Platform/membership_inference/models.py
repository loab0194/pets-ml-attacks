import numpy as np
import torch
import torchvision
import torchvision.transforms as transforms
import os
from PIL import Image
import glob
import torch.nn as nn
import torch.nn.functional as F
import pandas as pd

class Net_MNIST(nn.Module):
    def __init__(self):
        super(Net_MNIST, self).__init__()
        
        self.conv1 = nn.Conv2d(1, 16, 3, padding = 1)
        self.pool1 = nn.MaxPool2d(2, 2)
        self.batchnorm1 = nn.BatchNorm2d(16)
        
        self.conv2 = nn.Conv2d(16, 32, 3, padding = 1)
        self.pool2 = nn.MaxPool2d(2, 2)
        self.batchnorm2 = nn.BatchNorm2d(32)
        
        self.conv3 = nn.Conv2d(32, 64, 3, padding = 1)
        self.pool3 = nn.MaxPool2d(2, 2)
        self.batchnorm3 = nn.BatchNorm2d(64)
        
       
        self.fc1 = nn.Linear(576, 576)
        self.fc2 =  nn.Linear(576, 10)
        
        
    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = self.pool1(x)
        x = self.batchnorm1(x)
        
        x = F.relu(self.conv2(x))
        x = self.pool2(x)
        x = self.batchnorm2(x)
        
        x = F.relu(self.conv3(x))
        x = self.pool3(x)
        x = self.batchnorm3(x)
        
        x = x.view(-1, 576)
        x = F.relu(self.fc1(x))
        out = F.softmax(self.fc2(x), dim = 1)
        return out

class Net_CIFAR(nn.Module):
    def __init__(self):
        super(Net_CIFAR, self).__init__()
        
        self.conv1 = nn.Conv2d(3, 16, 3, padding = 1)
        self.pool1 = nn.MaxPool2d(2, 2)
        self.batchnorm1 = nn.BatchNorm2d(16)
        
        self.conv2 = nn.Conv2d(16, 32, 3, padding = 1)
        self.pool2 = nn.MaxPool2d(2, 2)
        self.batchnorm2 = nn.BatchNorm2d(32)
        
        self.conv3 = nn.Conv2d(32, 64, 3, padding = 1)
        self.pool3 = nn.MaxPool2d(2, 2)
        self.batchnorm3 = nn.BatchNorm2d(64)
        
       
        self.fc1 = nn.Linear(4 * 4 * 64, 1024)
        self.fc2 =  nn.Linear(1024, 10)
        
        
    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = self.pool1(x)
        x = self.batchnorm1(x)
        
        x = F.relu(self.conv2(x))
        x = self.pool2(x)
        x = self.batchnorm2(x)
        
        x = F.relu(self.conv3(x))
        x = self.pool3(x)
        x = self.batchnorm3(x)
        
        x = x.view(-1, 4 * 4 * 64)
        x = F.relu(self.fc1(x))
        out = F.softmax(self.fc2(x), dim = 1)
        return out
#FULLY CONNECTED MODEL FOR THE ATTACKER

class Net_Attack(nn.Module):
    def __init__(self):
        super(Net_Attack, self).__init__()
        
        self.fc1 = nn.Linear(10, 256)
        self.batchnorm1 = nn.BatchNorm1d(256)
        self.fc2 = nn.Linear(256, 1024)
        self.batchnorm2 = nn.BatchNorm1d(1024)
        self.fc3 = nn.Linear(1024, 128)
        self.batchnorm3 = nn.BatchNorm1d(128)
        self.fc4 = nn.Linear(128, 2)
        
        
    def forward(self, x):

        x = F.relu(self.fc1(x))
        x =  self.batchnorm1(x)
        x = F.relu(self.fc2(x))
        x =  self.batchnorm2(x)
        x = F.relu(self.fc3(x))
        x =  self.batchnorm3(x)
        out = F.softmax(self.fc4(x), dim = 1)
        return out

def get_attack_labels(dataset_type, dataset_path, BATCH_SIZE, attack_model, target_model):
    
    images_path = os.path.join(dataset_path,'*.jpg')
    counter = 0
    if dataset_type == 1:
        transform = transforms.Compose([transforms.ToTensor(),transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])
    elif dataset_type == 2:
        transform = transforms.Compose([transforms.ToTensor(),transforms.Normalize((0.5), (0.5))])
    attack_predictions = np.array([])
    
    
    
    true_predicted = pd.read_csv(os.path.join(dataset_path,'labels.txt'), delimiter = ' ', header = None, dtype = 'str')
    
    true_predicted = true_predicted.rename(columns = {0: 'image_names', 1 : 'Ground_Truth'})
    true_predicted['Ground_Truth'] = true_predicted['Ground_Truth'].astype(int)
    
       
    for filename in glob.glob(os.path.join(images_path)):
        
        
        im=Image.open(filename)
        im = transform(im)
        
        if dataset_type == 1:
            im = torch.reshape(im, (1, 3, 32, 32))
        elif dataset_type == 2:
            im = torch.reshape(im, (1, 1, 28, 28))
        

        if counter % BATCH_SIZE == 0:
            images = im
            img_name = []
            img_name.append(filename.split(os.sep)[-1].split('.')[-2])
            
        elif (counter + 1) % BATCH_SIZE  == 0:

            images = torch.cat((images, im), 0)
            img_name.append(filename.split(os.sep)[-1].split('.')[-2])
            
            posteriors = target_model(images)        
            membership_inference = attack_model(posteriors)
            _, membership_inference = torch.max(membership_inference, 1)
            membership_inference = membership_inference.numpy()
            attack_predictions = np.append(attack_predictions, membership_inference)
            
            
            imgs_data = pd.DataFrame(img_name, columns = {'image_names'})
            imgs_data['Predictions'] = membership_inference.tolist()
            
            
            true_predicted = true_predicted.merge(imgs_data, on = 'image_names', how = 'left')
            
            if counter > BATCH_SIZE:
                true_predicted = true_predicted.rename(columns = {'Predictions_x':'Predictions'})
                true_predicted['Predictions'] = true_predicted['Predictions'].fillna(true_predicted['Predictions_y'])
                true_predicted.drop(columns = {'Predictions_y'}, inplace = True)

        else:
            images = torch.cat((images, im), 0)
            img_name.append(filename.split(os.sep)[-1].split('.')[-2])
        

        counter = counter + 1            
    
    true_predicted = true_predicted.dropna(axis = 0, how = 'any')
    
    
    return true_predicted

def membership_inference(dataset_type, dataset_path, target_model_path):
    
    BATCH_SIZE = 4
    dataset_type = int(dataset_type)
    
    if dataset_type == 1:
        try:
            attack_model_path = "./membership_inference/attack_model_CIFAR.pt"
        except:
            attack_model_path = "./attack_model_CIFAR.pt"
        target_model = Net_CIFAR()
    elif dataset_type == 2:
        try:
            attack_model_path = "./membership_inference/attack_model_MNIST.pt"
        except: 
            attack_model_path = "./attack_model_MNIST.pt"
        target_model = Net_MNIST()
              
   
    target_model.load_state_dict(torch.load(target_model_path))
    target_model.eval()
    attack_model = Net_Attack()
    attack_model.load_state_dict(torch.load(attack_model_path))
    attack_model.eval()
    true_predicted = get_attack_labels(dataset_type, dataset_path, BATCH_SIZE, attack_model, target_model)
    print("=========================")
    print("Performing the attack...")

    print ("Accuracy is ", (true_predicted['Ground_Truth'] == true_predicted['Predictions']).sum()/len(true_predicted['Ground_Truth']))
    
    return true_predicted
