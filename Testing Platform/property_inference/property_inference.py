import numpy as np 
import pandas as pd
import os
import glob
import matplotlib.pyplot as plt
import seaborn as sns
import cv2          
import torch
from torch import nn, optim
import torchvision
from torchvision import transforms, datasets, models, utils
from torch.utils.data import Dataset, DataLoader 
from PIL import Image
from sklearn.model_selection import train_test_split
from torch.nn import functional as F
from skimage import io, transform
from torch.optim import lr_scheduler
from skimage.transform import AffineTransform, warp
from torchvision.transforms import transforms as tfs
from sklearn.metrics import classification_report
from tqdm import tqdm
import pickle 

try:
    from UTK_Labels import dataset_dict
except:
    from property_inference.UTK_Labels import dataset_dict

def parse_dataset(dataset_path, ext='jpg'):
    def parse_info_from_file(path):
        try:
            filename = os.path.split(path)[1]
            filename = os.path.splitext(filename)[0]
            age, gender, race, _ = filename.split('_')

            return int(age), dataset_dict['gender_id'][int(gender)], dataset_dict['race_id'][int(race)]
        except Exception as ex:
            return None, None, None
    files = glob.glob(os.path.join(dataset_path, "*.%s" % ext))
    records = []
    for file in files:
        info = parse_info_from_file(file)
        records.append(info)
        
    df = pd.DataFrame(records)
    df['file'] = files
    df.columns = ['age', 'gender', 'race', 'file']
    df = df.dropna()
    
    return df

class RaceClassifer(nn.Module):
    def __init__(self):
        resnet34 = models.resnet34(pretrained=True)
        super(RaceClassifer, self).__init__()
        self.features = nn.Sequential(*(list(resnet34.children())[:-2]))
        self.Flatten = nn.Flatten()
        self.droput = nn.Dropout(0.5)
        self.fc1 = nn.Linear(25088, 512)
        self.fc2 = nn.Linear(512, 128)
        self.fc3 = nn.Linear(128, 5)
    def forward(self, x):
        bs, _, _, _ = x.shape
        x = self.features(x)
        x = self.Flatten(x)
        x = self.droput(x)
        x = F.relu(self.fc1(x))
        x = self.droput(x)
        x1 = F.relu(self.fc2(x))
        return x1, self.fc3(x1)

class ShadowDataset(Dataset):
    def __init__(self, df, transform=None):
        self.x = df.drop(['age', 'gender', 'race'], axis=1)
        self.gender_y = df['gender']        
        self.transform = transform
        
    def __len__(self):
        return len(self.x)
    
    def __getitem__(self, idx):
        image=np.array(Image.open(self.x.iloc[idx]['file'])).astype(float)
        label=np.array([dataset_dict['gender_alias'][self.gender_y.iloc[idx]]]).astype('float')  
        if self.transform:
            image=self.transform(image)      
        sample={'image': np.uint8(image), 'label': label}
        return sample

class AttackModel(nn.Module):
    def __init__(self):
        super(AttackModel, self).__init__()
        self.fc1 = nn.Linear(128, 1000)
        self.fc2 = nn.Linear(1000, 500)
        self.fc3 = nn.Linear(500, 256)
        self.fc4 = nn.Linear(256, 2)
    def forward(self, x):
      x = F.relu(self.fc1(x))
      x = F.relu(self.fc2(x))
      x = F.relu(self.fc3(x))
      return self.fc4(x)

def print_classification_report_XGB(model, Trained_Model_Race, test_dataloader, device):
  y_pred = []
  y_true = []
  Trained_Model_Race.eval()
  for batch_idx, sample_batched in enumerate(test_dataloader):
      print("Processing batch {}/{}".format(batch_idx,len(test_dataloader)))
      image, label = sample_batched['image'].to(device), sample_batched['label'].to(device)
      feats ,_ = Trained_Model_Race(image.float())
      y_pred.extend(model.predict(feats.detach().cpu().numpy()))
      y_true.extend(label.squeeze().detach().cpu().numpy())
  target_names = ['male', 'female']
  print(classification_report(y_true, y_pred, target_names=target_names))

def print_classification_report_MLP(model, Trained_Model_Race, test_dataloader, device):
  y_pred = []
  y_true = []
  Trained_Model_Race.eval()
  for batch_idx, sample_batched in enumerate(test_dataloader):
      print("Processing batch {}/{}".format(batch_idx,len(test_dataloader)))
      image, label = sample_batched['image'].to(device), sample_batched['label'].to(device)
      feats ,_ = Trained_Model_Race(image.float())
      output = model(feats.float())         
      _, p = torch.max(output.data, 1)
      y_pred.extend(p.detach().cpu().numpy())
      y_true.extend(label.squeeze().detach().cpu().numpy())
  target_names = ['male', 'female']
  print(classification_report(y_true, y_pred, target_names=target_names))


def execute_attribute_inference(dataset_path="../../UTKFace/", target_model_path="./target_model.pt", attack_model_type="X"):
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print("Loading data..")
    df = parse_dataset(dataset_path)
    print("Loading Target model..")
    model_race = RaceClassifer().to(device)
    model_race.load_state_dict(torch.load(target_model_path))
    image_transform=tfs.Compose([tfs.ToTensor(),])
    transformed_test_data = ShadowDataset(df=df, transform=image_transform)
    test_dataloader = DataLoader(transformed_test_data, batch_size=50, shuffle=True, num_workers=4)
    print("Loading Attack model..")
    if(attack_model_type=="M"):
        attack_model = AttackModel().to(device)
        try:
            attack_model.load_state_dict(torch.load("./property_inference/attack_model.pt"))
        except:
            attack_model.load_state_dict(torch.load("./attack_model.pt"))
        print_classification_report_MLP(attack_model, model_race, test_dataloader, device)

    else:
        try:
            attack_model = pickle.load(open("./property_inference/pima.pickle.dat", "rb"))
        except:
            print("Please run this command: python3 -m pip install xgboost==0.90")
            return
        print_classification_report_XGB(attack_model, model_race, test_dataloader, device)
