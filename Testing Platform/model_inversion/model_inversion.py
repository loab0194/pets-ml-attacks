import torch
import torch.nn as nn
import json
import numpy as np
from matplotlib import pyplot as plt
import os

try:
  ATT_labels = open("model_inversion/ATT_labels.json")
except:
  ATT_labels = open("ATT_labels.json")
labels_map = json.load(ATT_labels)

class Classifier(torch.nn.Module):

    def __init__(self, input_size, num_classes):
        super(Classifier, self).__init__()
        self.block = nn.Sequential(
            nn.Linear(input_size, 3000),
            nn.Sigmoid(),
            nn.Linear(3000, num_classes),
        )
        self.linear1 = torch.nn.Linear(input_size, num_classes)

    def forward(self,x):
        return self.block(x)

def MI_Face(x, label, target_model_path, alpha=1000, lambda_=0.1):
    softmax_activation = nn.Softmax(dim=0)
    model = torch.load(target_model_path)
    model.eval()
    # Freeze target model weights
    for param in model.parameters():
      param.requires_grad = False
    # Start the attack
    c = labels_map[label]
    for i in range(alpha):
      x = torch.Tensor(x).view(1, -1)
      if not x.requires_grad:
          x.requires_grad = True
      pred = model(x)
      pr = softmax_activation(pred.squeeze())
      loss = 1- pr[c]
      # if i % 100 == 0:
      #   print('Loss: {}'.format(loss.item()))
      loss.backward()  
      x = x - lambda_ * x.grad
      x = x.detach().numpy()
    return  x
    
def execute_model_inversion(model_under_test):
  try:
    os.mkdir("./model_inversion_results")
  except:
    pass
  num_classes = 40
  input_picture_size = 10304
  for i in range(num_classes):
    label = 's' + str(i+1)
    print("Started the attack for identity {}".format(label))
    # Initializing an array that has the same size as input images
    x = np.zeros(input_picture_size)
    img = MI_Face(x, label, target_model_path=model_under_test, alpha=1000, lambda_=0.1)
    plt.imsave("./model_inversion_results/" + label + ".jpg", img.reshape(112, 92),cmap='gray')
  print("Deduced inputs are saved in \"model_inversion_results\"")
