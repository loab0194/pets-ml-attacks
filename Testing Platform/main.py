from model_inversion.model_inversion import execute_model_inversion, Classifier
from property_inference.property_inference import execute_attribute_inference, AttackModel, RaceClassifer
from membership_inference.models import membership_inference

while(1):
    options = "Please select an option (A/E/I/M):-\n" + \
              "I: Model Inversion\nM: Membership Inference\nA: Attribute Inference\nE: Exit\n"
    valid_options = ["E", "I", "M", "A"]
    try:
        attack = input(options)[0]
    except:
        continue
    if(attack.upper() not in valid_options):
        print("Please select a valid option! (A/E/I/M)\n")
    elif(attack.upper() == "E"):
        print("Thanks for using MPC! (Model Privacy Checker)")
        break    
    if(attack.upper() == "M"):
        #Execute Membership Inference
        target_model = input("Please enter the path for the target model:\n")
        dataset_type = input("Please enter 1 for CIFAR-10 or 2 for MNIST:\n")
        dataset = input("Please enter the path for the dataset:\n")
        membership_inference(dataset_type, dataset, target_model)
    if(attack.upper() == "A"):
        #Execute Property Inference
        target_model = input("Please enter the path for the target model:\n")
        dataset = input("Please enter the path for the dataset:\n")
        attack_model_type = input("Select Attack Model Classifier\nM: MLP (Faster)\nX: XGB Boost (Better Results)\n")
        execute_attribute_inference(dataset, target_model, attack_model_type)
    if(attack.upper() == "I"):
        #Execute Model Inversion
        target_model = input("Please enter the path for the target model:\n")
        execute_model_inversion(target_model)
